<?php
class Galleria extends CI_Controller {
    public function __construct() {
        parent::__construct();
    }
    
    public function index($kansio = "") {
        //Haetaan kaikki kansiot.
        $kansio = urldecode($kansio);
        $data["kansiot"] = $this->kansio_model->hae_kaikki();
        
        if (strlen($kansio) === 0 && count($data['kansiot']) > 0) {
            $kansio = $data['kansiot'][0];
            $kansio = substr($kansio, 0, strlen($kansio) - 1);
        }
        
        $data['valittu'] = $kansio;
        $this->session->set_userdata('kansio', $kansio);
        
        $data['kuvat'] = $this->kuva_model->hae_kansion_kuvat($kansio);
        
        //Asetetaan sivupalkki ja sisältönäkymät data-muuttujaan.
        $data["sivupalkki"] = "kansio/kansiot_view";
        $data["sisalto"] = "kuva/kuvat_view";
        
        //Näytetään template ja viedään tarvittavat tiedot näkymiin
        $this->load->view("template.php", $data);     
    }
    
    public function lisaa_kansio() {
        //Asetetaan oletusarvot (tyhjät) lomakkeen muuttujiin.
        $data = array(
            "nimi" => ""
        );
        
        //Haetaan näkymissä tarvittavat tiedot data-muuttujaan
        $data["kansiot"] = $this->kansio_model->hae_kaikki();
        $data["valittu"] = $this->session->userdata("kansio");
        $data["sivupalkki"] = "kansio/kansiot_view";
        $data["sisalto"] = "kansio/kansio_view";
        
        //Näytetään template ja viedään tarvittavat tiedot näkymiin data-muuttujassa (taulukko)
        $this->load->view("template.php", $data);
    }
    
    public function tallenna_kansio() {
        $kansio = $this->input->post("nimi");
        
        //Lisätään uusi kansio ja luetaan lisätyn kansion id paluuarvona muuttujaan
        $this->kansio_model->lisaa($kansio);
        
        //Uudelleenohjataan käyttäjä kansioiden selaussivulle ja välitetään juuri
        //lisätyn kansion id parametrina (jolloin se tulee valituksi).
        redirect("galleria/index/$kansio");
    }
    
    public function poista_kansio() {
        //Istuntomuuttuja sisältää valitun kansion id:n
        $kansio = $this->session->userdata("kansio");
        
        //Poistetaan kansio.
        $this->kansio_model->poista($kansio);
        
        //Poistetaan valitun kansion id istuntomuuttujasta, koska se juuri poistettiin.
        $this->session->unset_userdata("kansio");
        
        //Uudelleenohjataan käyttäjä kansioiden selaussivulle, jolloin haetaan
        //1. kansio valituksi (mikäli kansioita vielä on).
        redirect("galleria/index");
    }
    
    public function lisaa_kuva() {
        //Haetaan näkymissä tarvittavat tiedot data-muuttujaan.
        $data["kansiot"] = $this->kansio_model->hae_kaikki();
        $data["valittu"] = $this->session->userdata("kansio");
        $data["sivupalkki"] = "kansio/kansiot_view";
        $data["sisalto"] = "kuva/kuva_view";
        
        //Näytetään template ja viedään tarvittavat tiedot näkymiin
        //data-muuttujassa (taulukko).
        $this->load->view("template.php", $data);
    }
    
    public function lataa_kuva() {
        //Asetetaan konfiguraatio upload-kirjastolle.
        $kansio = $this->session->userdata("kansio");
        //$polku = $this->config->item("upload_path") . $kansio . "/";
        $this->kuva_model->lisaa($kansio);
        redirect("galleria/index/$kansio");
    }
    
    public function nayta_kuva($kuva) {
        $data["kuva"] = $kuva;
        $data["kansiot"] = $this->kansio_model->hae_kaikki();
        $data["valittu"] = $this->session->userdata("kansio");
        $data["sivupalkki"] = "kansio/kansiot_view";
        $data["sisalto"] = "kuva/esitys_view";
        
        // Naytetään template ja viedään tarvittavat tiedot näkymiin
        // data-muuttujassa (taulukko).
        $this->load->view("template.php", $data);
    }
    
    public function poista_kuva($kuva) {
        $kansio = $this->session->userdata("kansio");
        $this->kuva_model->poista($kansio, $kuva);
        redirect("galleria/index/$kansio");
    }
}