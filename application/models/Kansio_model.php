<?php
class Kansio_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->helper('directory');
    }
    
    public function hae_kaikki() {
        return array_keys(directory_map($this->config->item("upload_path")));
    }
    
    public function lisaa($kansio) {
        if (!mkdir($this->config->item("upload_path") . $kansio)) {
            throw new Exception("Virhe luotaessa kansiota. Kansion luominen ei onnistunut."); 
        }
    }
    
    public function poista($kansio) {
        if (!rmdir($this->config->item("upload_path") . $kansio)) {
            throw new Exception("Kansion poistaminen ei onnistunut. Tarkasta, että kansiossa ei ole kuvia.");
        }
    }
}