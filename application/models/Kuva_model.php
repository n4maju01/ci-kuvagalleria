<?php
class Kuva_model extends CI_Model {
    
    public function lisaa($kansio) {
        $polku = $this->config->item('upload_path') . '/' . $kansio . '/';
        chmod($polku, 0777);
        $tiedosto = $this->tallenna_kuva($polku);
        $this->luo_thumbnail($polku . $tiedosto);
        chmod($polku, 0755);
    }
    
    public function hae_kansion_kuvat($kansio) {
        return directory_map($this->config->item('upload_path') . '/' . $kansio . '/');
    }
    
    private function tallenna_kuva($polku) {
        $config['upload_path'] = $polku;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 5000;
        $config['max_width'] = 3000;
        $config['max_height'] = 2000;
        
        $this->load->library('upload', $config);
        
        if (!$this->upload->do_upload()) {
            throw new Exception($this->upload->display_errors());
        }
        
        $kuvan_tiedot = $this->upload->data();
        return $kuvan_tiedot['file_name'];
    }
    
    private function luo_thumbnail($tiedosto) {
        $config['create_thumb'] = TRUE;
        $config['image_library'] = 'gd2';
        $config['source_image'] = $tiedosto;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 100;
        $config['height'] = 100;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }
    
    public function poista($kansio, $kuva) {
        $polku = $this->config->item('upload_path') . "$kansio" . "/$kuva";
        chmod($polku, 0777);
        unlink($polku);
        // Palautetaan kuvan nimestä taulukko, jossa on kuvan nimi ja tiedostopääte omissa soluissaan.
        // Esim. $tiedosto[0]="koala" ja $tiedosto[1]=".jpg"
        $tiedosto = explode('.', $kuva);
        // Lisätään tiedostonimeen _thumb, jotta voidaan poistaa myös tämä kuva.
        $thumb_kuva = $tiedosto[0] . "_thumb." . $tiedosto[1];
        $polku_thumb = $this->config->item('upload_path') . "$kansio" . "/$thumb_kuva";
        chmod($polku_thumb, 0777);
        unlink($polku_thumb);
    }
}