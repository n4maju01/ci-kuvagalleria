<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Kuvagalleria</title>
        <link <?php print 'href=' . base_url() . 'css/style.css' ?> rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <nav>
                <h3>Kuvagalleria</h3>
                <div>
                    <a href="<?php echo site_url() . '/galleria/lisaa_kansio';?>">Lisää kansio</a> |
                    <a href="<?php echo site_url() . '/galleria/lisaa_kuva';?>">Lisää kuva</a> |
                    <a href="<?php echo site_url() . '/galleria/poista_kansio';?>">Poista kansio</a>
                </div>
            </nav>
            <div id="sivupalkki">
            <?php
            $this->load->view($sivupalkki);
            ?>
            </div>
            <div id="sisalto">
            <?php
            $this->load->view($sisalto);  
            ?>
            </div>
        </div>
  </body>
</html>

