<?php
if (!empty ($kuvat)) {
    foreach ($kuvat as $kuva) {
        if (strpos($kuva, "_thumb") > 0) {
            echo "<div class='kuva'>";
            echo "<a href='" . site_url() . "/galleria/nayta_kuva/" . urlencode($kuva) . "'>";
            echo img($this->config->item("upload_path") . '/' . $valittu . '/' . $kuva);
            echo "</a>";
            echo "</div>";
        }
    }
}