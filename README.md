# README #

http://pitkamatkaeipelota.fi/ci_kuvagalleria/

### FI ###

PHP:n CodeIgniter-frameworkilla väsätty kuvagalleria. Sisältää kansioiden ja kuvien lisäämisen ja poistamisen. Näkymiä on kaksi; pienet thumbnailit ja suuremmat esityskuvat. Käyttöliittymää ei ole kaunisteltu.

### EN ###

A PHP picture gallery done with CodeIgniter framework. Features adding and deleting of folders and pictures. Has two kinds of views; thumbnails and larger presentation pictures. The UI has not been beautified.